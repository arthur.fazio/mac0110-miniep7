# MAC0110 - MiniEP7
# Arthur Prado De Fazio - 9298614

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

function quaseigual(val1, val2)
	if abs(val2 - val1) < 0.001
		return true
	else
		return false
	end
end

function check_taylor_sin(value, x)
    return quaseigual(taylor_sin(x), value)
end

function check_taylor_cos(value, x)
    return quaseigual(taylor_cos(x), value)
end

function check_taylor_tan(value, x)
	return quaseigual(taylor_tan(x), value)
end

function taylor_sin(x)::BigFloat
	seno::BigFloat = 0
	i = 0
	termo::BigFloat = x

	while i <= 9
		seno = seno + termo
		i = i + 1
		termo = (-x * x * termo) / ((2 * i) * (2 * i + 1))
	end

	return seno
end

function taylor_cos(x)::BigFloat
	cosseno::BigFloat = 0
	i = 0
	termo::BigFloat = 1

	while i <= 9
		cosseno = cosseno + termo
		i = i + 1
		termo = (-x * x * termo) / ((2 * i - 1) * (2 * i))
	end
	
	return cosseno
end

function taylor_tan(x)::BigFloat
	tangente::BigFloat = 0
	i = 1
	pot_dois::BigInt = 4
	fatorial::BigInt = 2
	b_i::BigFloat = bernoulli(1)
	pot_x::BigFloat = x
	termo::BigFloat = (pot_dois * (pot_dois - 1) * b_i * pot_x) / (fatorial)

	while i <= 10
		tangente = tangente + termo
		i = i + 1
		pot_dois = pot_dois * 4
		fatorial = fatorial * (2 * i) * (2 * i - 1)
		b_i = bernoulli(i)
		pot_x = pot_x * x * x
		termo = (pot_dois * (pot_dois - 1) * b_i * pot_x) / (fatorial) 
	end

	return tangente
end

function test()
	if !check_taylor_sin(sin(π/6), π/6)
	    println("A função taylor_sin não funciona para x = π/6")
	end
	if !check_taylor_sin(sin(π/4), π/4)
	    println("A função taylor_sin não funciona para x = π/4")
	end
	if !check_taylor_sin(sin(π/3), π/3)
	    println("A função taylor_sin não funciona para x = π/3")
	end
	if !check_taylor_cos(cos(π/6), π/6)
	    println("A função taylor_cos não funciona para x = π/6")
	end
	if !check_taylor_cos(cos(π/4), π/4)
	    println("A função taylor_cos não funciona para x = π/4")
	end
	if !check_taylor_cos(cos(π/3), π/3)
	    println("A função taylor_cos não funciona para x = π/3")
	end
	println("Final dos testes")
end